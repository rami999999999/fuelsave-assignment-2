import sqlite3

def initiate(dbpath, files_path):
    conn = sqlite3.connect(dbpath)
    c = conn.cursor()

    c.execute('''CREATE TABLE users
        (userid integer not null primary key, 
        user text not null unique, 
        password text not null unique)''')

    c.execute('''CREATE TABLE cars (
        carid integer not null primary key, distance integer,
        owner integer not null,
        foreign key(owner) references users(userid))''')

    c.execute('''CREATE TABLE tokens (
        tokenid integer not null primary key,
        user integer,
        token text,
        timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
        foreign key (user) references users(userid))''')
   
    users = open(files_path + "/users.csv", "r")
    cars = open(files_path + "/vehicles.csv", "r")

    users_insert_sql = 'INSERT INTO users (user, password) VALUES (?,?)'

    cars_insert_sql = '''INSERT INTO cars (carid, distance, owner) 
        VALUES (?,?,?)'''
    
    cars_find_ownerid = 'SELECT userid from users where user=?'
    
    users.readline()
    for user in users:
        user = user.rstrip()
        data = user.split(",")
        c.execute(users_insert_sql, (data[0], data[1]))

    cars.readline()
    for car in cars:
        car = car.rstrip()
        data = car.split(",")
        records = c.execute(cars_find_ownerid, (data[2],))
       
        ownerid = records.fetchone()[0]
        
        c.execute(cars_insert_sql, (data[0], data[1], ownerid)) 

    conn.commit()

    users.close()
    cars.close()

if __name__ == '__main__':
    initiate('/data/users_cars.db', '/data/')


