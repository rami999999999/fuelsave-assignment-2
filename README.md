I've used sqlite, which is a serveless database. For that reason I didn't need to add different container for it.

I haven't have the decision to run from docker-compose :P. Using mariadb would require something like the following into docker compose:

```
  mariadb:
    image: mariadb:10.2
    environment:
      MYSQL_ROOT_PASSWORD: changeme
      MYSQL_DATABASE: mybb
      MYSQL_USER: mybb
      MYSQL_PASSWORD: changeme
    restart: on-failure
    volumes:
     - ${PWD}/mariadb:/var/lib/mysql
```
Queries should be similar, the great difference will be on the connect, which will be IP based instead of a local filesystem connect.

I believe software should be contructed with simplicity and then optimize for performance if needed. Since I don't know the usage and load in the database I assumed sqlite will be sufficient.

In case of performance problems, that should be one of the possible bottlenecks.

Another challange was the encryption part.
I believe the best way to secure the application connection is to use https. It does not guarantee authentification though.

What I did was use something which was both known to client and server to share encrypt the token.

The token is then used to encrypt the remaining communication.

This is based on a simetric encryption. The assimetric was considered but it would require to exchange public keys for it to work, which didn't fit on the REST interface.


Because we only recieve an encrypted user, we have no way to know which user is really trying to get the vehicles until we find the right token. So I had to go through all of the tokens in the database to find one which decrypts into a valid user.
This is the point in the application which might need an optimzation if the performance is not enough. 

Regarding the token validity, I check it but I don't delete the old tolkens. Maybe some kind of cleanup would be needed in a cron like action.

Finaly the unit-tests, I tested some of the queries and the encryption/decryption functions.
I believe I could extend the tests to catch corner cases, or add tests to even more queries (not all are being tested).

I did took some more time to finish than the 6 hours, for which I feel disapointed. Find the best way to handle the security was for sure what took longer for me.


I've added an ultra simple makefile which runs as follows:

+ make build: builds images
+ make run: starts the docker-compose services
+ make test: initiates the unit-tests (doesn't need the service to be running)




