import os
import unittest
from cryptography.fernet import Fernet
from data.init_sqlite import *
from app.functions import *




class TestQueries(unittest.TestCase):
    
    def setUp(self):
        initiate("./users_cars.db", "./data/")

    def tearDown(self):
        os.remove("./users_cars.db")

    def test_check_user(self):
        conn = sqlite3.connect("./users_cars.db")
        c = conn.cursor()
        self.assertEqual(check_user(c, "user1", "pass1"), 1)

    def test_find_userid(self):
        conn = sqlite3.connect("./users_cars.db")
        c = conn.cursor()
        self.assertEqual(find_userid(c, "user1"), 1)


    def get_add_token(self):
        conn = sqlite3.connect("./users_cars.db")
        c = conn.cursor()
        add_token(c, "aaa", 1)
        records = c.execute('select token, user from tokens where token = "aaa"')

        self.assertEqual(records.fetchone(), ("aaa",1))

class TestEncryption(unittest.TestCase):

    def test_encryption(self):
        username = "user1"
        password = "pass1"
        token = Fernet.generate_key()

        enc_token = get_enc_token(username, password, token)
        key = generate_hash(username, password)

        message = decrypt(enc_token, key)

        self.assertEqual(message, token)


if __name__ == '__main__':
    unittest.main()





