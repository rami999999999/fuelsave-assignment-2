FROM tiangolo/uvicorn-gunicorn:python3.7

LABEL maintainer="Fuelsave <areis@fuelsave.io>"

### Copy over and install the requirements
COPY ./app/requirements.txt $DIR/
RUN pip install -r $DIR/requirements.txt

COPY ./data/ /data
RUN python3 /data/init_sqlite.py

RUN rm /data/*.csv

COPY ./app /app

#ENTRYPOINT ["python3", "/app/main.py"]

WORKDIR /
ENTRYPOINT ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080"]

