import uvicorn

from fastapi import FastAPI
from starlette.responses import RedirectResponse
from pydantic import BaseModel

from cryptography.fernet import Fernet
from base64 import b64decode, b64encode

import sqlite3
import secrets
import hashlib

from functions import *

app = FastAPI()


class Token(BaseModel):
    username: str
    password: str


@app.get("/", include_in_schema=False)
async def read_root():
    return RedirectResponse(url='/docs')

@app.post("/token/")
def generate_token(item: Token):
    username = item.username
    password = item.password
    # open connection to DB
    conn = sqlite3.connect("/data/users_cars.db")
    c = conn.cursor()
    
    if check_user(c, username, password) == 1:
        
        # token will be used as key to encrypt the username for vehicles endpoint
        token = Fernet.generate_key() 
        
        
        enc_token = get_enc_token(username, password, token)
        
        userid = find_userid(c, username)
        
        add_token(c, token, userid)
        
        conn.commit()
        
        return enc_token
    else:
        return "Authentification Failed"

@app.get("/vehicles")
def get_vehicles(enc_user):

    # queries to be used
    get_tokens_sql = "select * from tokens where \
            timestamp < datetime('now','+10 minutes') "
    get_user_sql = "select user from users, tokens \
            where tokens.user = users.userid and tokens.token = ?"
    get_vehichles_sql = "select carid, distance from cars, users \
            where cars.owner = users.userid and \
            user.userid = ? \
            order by distance desc"

    conn = sqlite3.connect("/data/users_cars.db")
    c = conn.cursor()
    
    # retrieve all tokens
    # it will go through all of them trying to find a good one, it is not very
    # performant but it is the best way I could find which guarantees some 
    # security
    token_records = c.execute(get_tokens_sql)

    for t_rows in token_records.fetchall():
        # msg_user is the user which we get by trying to decrypt with
        # the token we are getting from the DB
        msg_user = decrypt(enc_user, t_row[2]) 

        # db_user is the user which is associated with the token in the DB
        db_user = c.execute(get_user_sql, (t_row[2],)).fetchone[0]

        if db_user != msg_user:
            continue
        else:
            car_records = c.execute(get_vehicles_sql, (msg_user,))
            cars = ""
            for car_row in records.fetchall():
                cars = car + f"{car_row[0]}:\t{car_row[1]}\n"
            return cars 
        
    return "Invalid token"

if __name__ == '__main__':
    # only debug
    uvicorn.run(app, host="0.0.0.0", port=8080)
