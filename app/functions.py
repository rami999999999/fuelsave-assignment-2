import sqlite3

from cryptography.fernet import Fernet
from base64 import b64decode, b64encode
import hashlib

# check how many users in the database for given user and password
def check_user(cursor, username, password):
    check_user_sql = 'SELECT user, password from \
            users where user = ? and password = ?'
    
    result = cursor.execute(check_user_sql, (username, password))

    return len(result.fetchall())

# get userid given the username
def find_userid(cursor, user):
    find_ownerid = ''' SELECT userid from users where user=?'''
    records = cursor.execute(find_ownerid, (user,))
    userid = records.fetchone()[0]
    return userid

# add token to the database
def add_token(cursor, token, userid):
    add_token_sql = 'INSERT INTO tokens (token, user) VALUES (?, ?)'
    cursor.execute(add_token_sql, (token, userid))


# The hash is used as common knowledge since both client and 
# server know it and consists of the hash of a string which is a 
#concatenation of the username, the password and some salt
def generate_hash(username, password):
        to_hash = 'AUTHOK_' + username + password
        to_hash = to_hash.encode('utf-8')

        key = hashlib.sha3_256(to_hash).digest()
        key = b64encode(key)
        return key

# returns and encrypted token using the hash from generate_hash function
def get_enc_token(username, password, token):
        key = generate_hash(username, password)
        f = Fernet(key)
        return f.encrypt(token)

# decrypts some text using key
def decrypt(text, key):
    f = Fernet(key)
    return f.decrypt(text) 
